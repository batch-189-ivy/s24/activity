
const firstNum = 2 ** 2;
console.log(`The cube of 2 is ${firstNum}`)



let address = [ "258", "Washington Ave NW", "California 90011"]
	
const [House, City, State] = address
console.log(`I live at ${House} ${City} ${State}`)




const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

function animalDetails({name, type, weight, measurement}) {
	console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`)
}

animalDetails(animal)